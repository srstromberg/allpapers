package com.stromberg.scott.allpapers.model;

public class SelectFolderResult {
  public enum FolderType {
    DROPBOX,
    GOOGLE_DRIVE;

    @Override
    public String toString() {
      switch(this) {
        case DROPBOX: return "dropbox";
        case GOOGLE_DRIVE: return "google drive";
        default: throw new IllegalArgumentException();
      }
    }
  }

  public FolderType folderType;
  public String folderPath;

  public SelectFolderResult(FolderType folderType, String folderPath) {
    this.folderType = folderType;
    this.folderPath = folderPath;
  }
}
