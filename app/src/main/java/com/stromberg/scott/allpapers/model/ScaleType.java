package com.stromberg.scott.allpapers.model;

public enum ScaleType {
  FIT,
  CENTER;

  public int getAsInt() {
    switch(this) {
      case FIT: return 1;
      case CENTER: return 2;
      default: throw new IllegalArgumentException();
    }
  }
}
