package com.stromberg.scott.allpapers.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.google.gson.Gson;
import com.stromberg.scott.allpapers.R;
import com.stromberg.scott.allpapers.dropbox.DropboxWallpaperUpdater;
import com.stromberg.scott.allpapers.model.SelectFolderResult;

public class TimerReceiver extends BroadcastReceiver {
  private TimerService mTimerService;

  public TimerReceiver() {}

  @Override
  public void onReceive(Context context, Intent intent) {
    SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.shared_prefs_key), context.MODE_PRIVATE);
    int requestCode = intent.getExtras().getInt("RequestCode", -1);

    if(requestCode == TimerService.CHANGE_WALLPAPER) {
      ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
      NetworkInfo wifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

      boolean onlyUpdateOnWifi = prefs.getBoolean(context.getString(R.string.prefs_update_on_wifi), true);

      if (wifi.isConnected() || !onlyUpdateOnWifi) {
        String selectedFolderResultJson = prefs.getString(context.getString(R.string.prefs_selected_folder_result), null);
        SelectFolderResult selectedFolderResult = new Gson().fromJson(selectedFolderResultJson, SelectFolderResult.class);

        if (selectedFolderResult != null) {
          Log.d("Allpapers", "Updating wallpaper");

          if (selectedFolderResult.folderType == SelectFolderResult.FolderType.DROPBOX) {
            new DropboxWallpaperUpdater(context).update(selectedFolderResult);
          }
        }
      }
      else {
        Log.d("Allpapers", "Not updating: must be on wifi");
      }
    }
    else if(requestCode == TimerService.RESTART_TIMER) {
      if(TimerService.isRunning) {
        context.stopService(new Intent(context, TimerService.class));
      }

      Intent svc = new Intent(context, TimerService.class);
      context.startService(svc);
    }
  }
}
