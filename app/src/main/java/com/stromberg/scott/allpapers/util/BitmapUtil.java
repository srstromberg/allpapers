package com.stromberg.scott.allpapers.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.support.v7.graphics.Palette;
import android.util.Log;

public class BitmapUtil {
  public static Bitmap createFitBitmap(Bitmap image, float desiredWidth, float desiredHeight, boolean scaleDown) {
    Bitmap bitmap = scale(image, desiredWidth, desiredHeight, scaleDown);

    int dominantColor = Palette.from(bitmap).generate().getDominantColor(Color.BLACK);

    float widthAdjust, heightAdjust;
    if(Math.min(desiredWidth, desiredHeight) == desiredWidth) {
      widthAdjust = 0;
      heightAdjust = Math.abs(desiredHeight - bitmap.getHeight()) / 2;
    }
    else {
      heightAdjust = 0;
      widthAdjust = Math.abs(desiredWidth - bitmap.getWidth()) / 2;
    }

    Bitmap newBitmap = Bitmap.createBitmap((int)desiredWidth, (int)desiredHeight, bitmap.getConfig());
    Canvas canvas = new Canvas();
    canvas.setBitmap(newBitmap);
    canvas.drawColor(dominantColor);

    canvas.drawBitmap(bitmap, widthAdjust, heightAdjust, null);

    return newBitmap;
  }

  public static Bitmap createCenterBitmap(Bitmap wallpaper, float desiredWidth, float desiredHeight) {
    float dimensionToUse = 0;
    float scale = 1;

    if(desiredWidth > desiredHeight) {
      dimensionToUse = desiredWidth;
      scale = dimensionToUse / wallpaper.getWidth();
    }
    else {
      dimensionToUse = desiredHeight;
      scale = dimensionToUse / wallpaper.getHeight();
    }

    float scaledWidth = scale * (float)wallpaper.getWidth();
    float scaledHeight = scale * (float)wallpaper.getHeight();

    if(scaledWidth < desiredWidth) {
      // scale up
      scale = desiredWidth / scaledWidth;

      scaledWidth = scale * scaledWidth;
      scaledHeight = scale * scaledHeight;
    }
    else if(scaledHeight < desiredHeight) {
      // scale up
      scale = desiredHeight / scaledHeight;

      scaledWidth = scale * scaledWidth;
      scaledHeight = scale * scaledHeight;
    }

    int widthToTrim = (int)((scaledWidth - desiredWidth) / 2);
    int heightToTrim = (int)((scaledHeight - desiredHeight) / 2);

    Bitmap scaledWallpaper = Bitmap.createScaledBitmap(wallpaper, (int)scaledWidth, (int)desiredHeight, true);
    Bitmap croppedWallpaper = Bitmap.createBitmap(
      scaledWallpaper,
      widthToTrim,
      heightToTrim,
      (int)desiredWidth,
      (int)desiredHeight
    );

    return croppedWallpaper;
  }

  private static Bitmap scale(Bitmap image, float desiredWidth, float desiredHeight, boolean scaleDown) {
    float newWidth, newHeight;
    float screenToImageRatio;

    float widthToUse = scaleDown ? Math.min(desiredWidth, desiredHeight) : Math.max(desiredWidth, desiredHeight);

    if(widthToUse == desiredWidth) {
      screenToImageRatio = desiredWidth / image.getWidth();
      newWidth = image.getWidth() * screenToImageRatio;
      newHeight = image.getHeight() * screenToImageRatio;
    }
    else {
      screenToImageRatio = desiredHeight / image.getHeight();
      newHeight = image.getHeight() * screenToImageRatio;
      newWidth = image.getWidth() * screenToImageRatio;
    }

    Log.d("Allpapers", "ow: " + image.getWidth() + ", oh: " + image.getHeight() + ", nw: " + newWidth + ", nh: " + newHeight + ", screenToImageRatio: " + screenToImageRatio);

    Bitmap newBitmap = Bitmap.createScaledBitmap(image, (int) newWidth, (int) newHeight, true);
    return newBitmap;
  }
}
