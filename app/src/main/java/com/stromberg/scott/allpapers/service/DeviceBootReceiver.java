package com.stromberg.scott.allpapers.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class DeviceBootReceiver extends BroadcastReceiver {
  @Override
  public void onReceive(Context context, Intent intent) {
    Intent svc = new Intent(context, TimerService.class);
    context.startService(svc);
  }
}