package com.stromberg.scott.allpapers.dropbox;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.Metadata;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.stromberg.scott.allpapers.R;
import com.stromberg.scott.allpapers.model.SelectFolderResult;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DropboxFilesAdapter extends RecyclerView.Adapter<DropboxFilesAdapter.MetadataViewHolder> {
    private List<Metadata> mFiles;
    private Context mContext;
    private final Picasso mPicasso;
    private final Callback mCallback;
    private SelectFolderResult mSelectedFolderResult;
    public MetadataViewHolder selectedFolder;

    public void setFiles(List<Metadata> files) {
        mFiles = Collections.unmodifiableList(new ArrayList<>(files));
        notifyDataSetChanged();

        SharedPreferences prefs = mContext.getSharedPreferences(mContext.getString(R.string.shared_prefs_key), mContext.MODE_PRIVATE);
        String selectedFolderResultJson = prefs.getString(mContext.getString(R.string.prefs_selected_folder_result), null);
        mSelectedFolderResult = new Gson().fromJson(selectedFolderResultJson, SelectFolderResult.class);
    }

    public interface Callback {
        void onFolderClicked(FolderMetadata folder);
        void onFileClicked(FileMetadata file);
    }

    public DropboxFilesAdapter(Context context, Picasso picasso, Callback callback) {
        mContext = context;
        mPicasso = picasso;
        mCallback = callback;
    }

    @Override
    public MetadataViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        View view = LayoutInflater.from(context)
                .inflate(R.layout.files_item, viewGroup, false);
        return new MetadataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MetadataViewHolder metadataViewHolder, int i) {
        metadataViewHolder.bind(mFiles.get(i));
    }

    @Override
    public long getItemId(int position) {
        return mFiles.get(position).getPathLower().hashCode();
    }

    @Override
    public int getItemCount() {
        return mFiles == null ? 0 : mFiles.size();
    }

    public class MetadataViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final TextView textView;
        public final ImageView imageView;
        public final CheckBox checkbox;
        public Metadata item;

        public MetadataViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            textView = (TextView) itemView.findViewById(R.id.text);
            checkbox = (CheckBox) itemView.findViewById(R.id.checkbox);
            itemView.setOnClickListener(this);

            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (selectedFolder != null) {
                        selectedFolder.checkbox.setChecked(false);
                    }

                    SharedPreferences prefs = mContext.getSharedPreferences(mContext.getString(R.string.shared_prefs_key), mContext.MODE_PRIVATE);
                    if (isChecked) {
                        selectedFolder = MetadataViewHolder.this;
                        prefs.edit().putString(mContext.getString(R.string.prefs_selected_folder_result), new Gson().toJson(new SelectFolderResult(SelectFolderResult.FolderType.DROPBOX, selectedFolder.item.getPathLower()))).apply();
                    } else {
                        selectedFolder = null;
                        prefs.edit().remove(mContext.getString(R.string.prefs_selected_folder_result)).apply();
                    }

                }
            });
        }

        @Override
        public void onClick(View v) {
            if (item instanceof FolderMetadata) {
                mCallback.onFolderClicked((FolderMetadata) item);
            }  else if (item instanceof FileMetadata) {
                mCallback.onFileClicked((FileMetadata) item);
            }
        }

        public void bind(Metadata item) {
            this.item = item;
            textView.setText(this.item.getName());

            if(mSelectedFolderResult != null && mSelectedFolderResult.folderPath != null && mSelectedFolderResult.folderPath.equalsIgnoreCase(item.getPathLower())) {
                checkbox.setChecked(true);
            }

            if (item instanceof FileMetadata) {
                MimeTypeMap mime = MimeTypeMap.getSingleton();
                String ext = item.getName().substring(item.getName().indexOf(".") + 1);
                String type = mime.getMimeTypeFromExtension(ext);
                if (type != null && type.startsWith("image/")) {
                    mPicasso.load(FileThumbnailRequestHandler.buildPicassoUri((FileMetadata)item))
                            .placeholder(R.drawable.ic_photo_grey_600_36dp)
                            .error(R.drawable.ic_photo_grey_600_36dp)
                            .into(imageView);
                } else {
                    mPicasso.load(R.drawable.ic_insert_drive_file_blue_36dp)
                            .noFade()
                            .into(imageView);
                }
            } else if (item instanceof FolderMetadata) {
                mPicasso.load(R.drawable.ic_folder_blue_36dp)
                        .noFade()
                        .into(imageView);
            }
        }
    }
}
