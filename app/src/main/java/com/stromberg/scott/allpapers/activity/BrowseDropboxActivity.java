package com.stromberg.scott.allpapers.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.FolderMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.google.gson.Gson;
import com.stromberg.scott.allpapers.R;
import com.stromberg.scott.allpapers.dropbox.DropboxAuth;
import com.stromberg.scott.allpapers.dropbox.DropboxClientFactory;
import com.stromberg.scott.allpapers.dropbox.DropboxFilesAdapter;
import com.stromberg.scott.allpapers.dropbox.ListFolderTask;
import com.stromberg.scott.allpapers.dropbox.PicassoClient;
import com.stromberg.scott.allpapers.model.SelectFolderResult;

import java.util.ArrayList;
import java.util.Stack;

public class BrowseDropboxActivity extends AppCompatActivity {
  private DropboxFilesAdapter mFilesAdapter;
  private Stack<String> mPath = new Stack<>();
  private Stack<String> mDisplayPath = new Stack<>();
  private boolean initialized = false;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.browse_dropbox_activity);

    Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    ((TextView) toolbar.getChildAt(0)).setEllipsize(TextUtils.TruncateAt.MIDDLE);

    if(DropboxAuth.getOAuth2Token(BrowseDropboxActivity.this) == null) {
      DropboxAuth.startOAuth2Authentication(BrowseDropboxActivity.this);
    }
    else {
      init();
    }
  }

  @Override
  public void onResume() {
    super.onResume();

    if(DropboxAuth.getOAuth2Token(BrowseDropboxActivity.this) != null && !initialized) {
      init();
    }
  }

  @Override
  public void onBackPressed() {
    onSupportNavigateUp();
  }

  @Override
  public boolean onSupportNavigateUp() {
    mPath.pop();
    mDisplayPath.pop();

    if(mPath.size() > 0) {
      getFolderContents(mPath.peek(), mDisplayPath.peek(), false);
    }
    else {
      finish();
    }

    return true;
  }

  private void init() {
    initialized = true;
    DropboxClientFactory.init(DropboxAuth.getOAuth2Token(BrowseDropboxActivity.this));
    PicassoClient.init(getApplicationContext(), DropboxClientFactory.getClient());

    RecyclerView recyclerView = (RecyclerView) findViewById(R.id.files_list);
    mFilesAdapter = new DropboxFilesAdapter(BrowseDropboxActivity.this, PicassoClient.getPicasso(), new DropboxFilesAdapter.Callback() {
      @Override
      public void onFolderClicked(FolderMetadata folder) {
        getFolderContents(folder.getPathLower(), folder.getPathDisplay(), true);
      }

      @Override
      public void onFileClicked(final FileMetadata file) {

      }
    });
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
    recyclerView.setAdapter(mFilesAdapter);

    findViewById(R.id.select_folder_button).setOnClickListener(view -> {
      if(mFilesAdapter.selectedFolder != null || mFilesAdapter.getItemCount() == 0) {
        String path = "";

        if(mFilesAdapter.selectedFolder != null) {
          path = mFilesAdapter.selectedFolder.item.getPathLower();
        }
        else {
          path = mPath.peek();
        }

        Intent intent = new Intent();
        intent.putExtra(getString(R.string.prefs_selected_folder_result), new Gson().toJson(new SelectFolderResult(SelectFolderResult.FolderType.DROPBOX, path)));
        setResult(MainActivity.FOLDER_SELECTED, intent);
        finish();
      }
      else {
        Snackbar.make(recyclerView, R.string.please_select_a_folder, 3000).show();
      }
    });

    getFolderContents("", "/", true);
  }

  private void getFolderContents(final String path, final String displayPath, boolean addToStack) {
    if(addToStack) {
      mPath.push(path);
      mDisplayPath.push(displayPath);
    }

    final ProgressDialog dialog = new ProgressDialog(this);
    dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    dialog.setCancelable(false);
    dialog.setMessage(getString(R.string.loading));
    dialog.show();

    new ListFolderTask(DropboxClientFactory.getClient(), new ListFolderTask.Callback() {
      @Override
      public void onDataLoaded(ListFolderResult result) {
        dialog.dismiss();

        ArrayList<Metadata> entries = new ArrayList<>();

        result.getEntries().forEach((metaData)->{ if(metaData instanceof FolderMetadata) { entries.add(metaData); } });

        mFilesAdapter.setFiles(entries);

        getSupportActionBar().setTitle(displayPath);
      }

      @Override
      public void onError(Exception e) {
        dialog.dismiss();

        Toast.makeText(BrowseDropboxActivity.this,
            "An error has occurred",
            Toast.LENGTH_SHORT)
            .show();
      }
    }).execute(path);
  }
}
