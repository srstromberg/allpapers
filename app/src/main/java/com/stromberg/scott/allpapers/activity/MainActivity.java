package com.stromberg.scott.allpapers.activity;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.google.gson.Gson;
import com.stromberg.scott.allpapers.R;
import com.stromberg.scott.allpapers.dropbox.DownloadFileTask;
import com.stromberg.scott.allpapers.dropbox.DropboxAuth;
import com.stromberg.scott.allpapers.dropbox.DropboxClientFactory;
import com.stromberg.scott.allpapers.dropbox.ListFolderTask;
import com.stromberg.scott.allpapers.dropbox.PicassoClient;
import com.stromberg.scott.allpapers.model.ScaleType;
import com.stromberg.scott.allpapers.model.SelectFolderResult;
import com.stromberg.scott.allpapers.service.TimerReceiver;
import com.stromberg.scott.allpapers.service.TimerService;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    public TextView selectedFolderTextView;
    public static final int SELECT_FOLDER = 2000;
    public static final int FOLDER_SELECTED = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main_activity);

        selectedFolderTextView = (TextView) findViewById(R.id.selected_folder);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        final SharedPreferences prefs = getSharedPreferences(getString(R.string.shared_prefs_key), MODE_PRIVATE);

        findViewById(R.id.reset_button).setOnClickListener(view -> {
            new AlertDialog.Builder(MainActivity.this)
                .setMessage(R.string.reset_selected_folder)
                .setCancelable(true)
                .setPositiveButton(getString(R.string.reset), (dialog, which) -> {
                    getSharedPreferences(getString(R.string.shared_prefs_key), MODE_PRIVATE).edit().remove(getString(R.string.prefs_selected_folder_result)).apply();
                    selectedFolderTextView.setText(getString(R.string.none));

                    dialog.dismiss();
                })
                .setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.dismiss())
                .create().show();
        });

        findViewById(R.id.browse_dropbox_button).setOnClickListener(view -> {
            startActivityForResult(new Intent(MainActivity.this, BrowseDropboxActivity.class), SELECT_FOLDER);
        });

        findViewById(R.id.browse_google_drive_button).setOnClickListener(view -> {
            Toast.makeText(this, "Not available yet", Toast.LENGTH_SHORT).show();
        });

        int[] changeIntervalValues = getResources().getIntArray(R.array.change_interval_values);
        int changeInterval = prefs.getInt(getString(R.string.prefs_wallpaper_change_interval), Integer.parseInt(getString(R.string.default_wallpaper_change_interval)));

        for(int i = 0; i < changeIntervalValues.length; i++) {
            if(changeIntervalValues[i] == changeInterval) {
                ((TextView) findViewById(R.id.change_interval_text_view)).setText(getResources().getStringArray(R.array.change_interval_display)[i]);
                break;
            }
        }

        findViewById(R.id.change_interval_container).setOnClickListener(v -> findViewById(R.id.change_interval_spinner).performClick());

        for(int i = 0; i < changeIntervalValues.length; i++) {
            if(changeIntervalValues[i] == changeInterval) {
                ((Spinner) findViewById(R.id.change_interval_spinner)).setSelection(i);
                break;
            }
        }

        ((Spinner) findViewById(R.id.change_interval_spinner)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Allpapers", "Interval selected: " + getResources().getStringArray(R.array.change_interval_display)[position]);
                prefs.edit().putInt(getString(R.string.prefs_wallpaper_change_interval), changeIntervalValues[position]).apply();
                ((TextView) findViewById(R.id.change_interval_text_view)).setText(getResources().getStringArray(R.array.change_interval_display)[position]);

                restartTimer();
            }

            @Override public void onNothingSelected(AdapterView<?> parent) {}
        });

        boolean onlyUpdateOnWifi = prefs.getBoolean(getString(R.string.prefs_update_on_wifi), true);
        ((CheckBox) findViewById(R.id.wifi_only_checkbox)).setChecked(onlyUpdateOnWifi);
        ((CheckBox) findViewById(R.id.wifi_only_checkbox)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isSelected) {
                prefs.edit().putBoolean(getString(R.string.prefs_update_on_wifi), isSelected).apply();

                restartTimer();
            }
        });

        int scaleType = prefs.getInt(getString(R.string.prefs_scale_type), ScaleType.CENTER.getAsInt());
        if(scaleType == ScaleType.FIT.getAsInt()) {
            ((RadioButton)findViewById(R.id.scale_type_fit)).setChecked(true);
        }
        else if(scaleType == ScaleType.CENTER.getAsInt()) {
            ((RadioButton)findViewById(R.id.scale_type_center)).setChecked(true);
        }

        ((RadioButton)findViewById(R.id.scale_type_fit)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    prefs.edit().putInt(getString(R.string.prefs_scale_type), ScaleType.FIT.getAsInt()).apply();
                }
            }
        });

        ((RadioButton)findViewById(R.id.scale_type_center)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    prefs.edit().putInt(getString(R.string.prefs_scale_type), ScaleType.CENTER.getAsInt()).apply();
                }
            }
        });

        String selectedFolderResultJson = prefs.getString(getString(R.string.prefs_selected_folder_result), null);

        if(selectedFolderResultJson != null) {
            SelectFolderResult selectedFolderResult = new Gson().fromJson(selectedFolderResultJson, SelectFolderResult.class);
            selectedFolderTextView.setText(selectedFolderResult.folderPath + " " + "(" + selectedFolderResult.folderType.toString() + ")");
        }

        setUpStoragePermission();

        if(!TimerService.isRunning) {
            Intent svc = new Intent(MainActivity.this, TimerService.class);
            startService(svc);
        }
    }

    private void restartTimer() {
        Log.d("Allpapers", "Restarting timer and service");

        Intent intent = new Intent(MainActivity.this, TimerReceiver.class);
        intent.setAction(getString(R.string.filter_action_restart_timer));
        intent.putExtra("RequestCode", TimerService.RESTART_TIMER);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(MainActivity.this, TimerService.RESTART_TIMER, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager mainUpdateManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mainUpdateManager.set(AlarmManager.RTC, Calendar.getInstance().getTimeInMillis(), pendingIntent);
    }


    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences prefs = getSharedPreferences(getString(R.string.shared_prefs_key), MODE_PRIVATE);
        String selectedFolderResultJson = prefs.getString(getString(R.string.prefs_selected_folder_result), null);

        if(selectedFolderResultJson != null) {
            SelectFolderResult selectedFolderResult = new Gson().fromJson(selectedFolderResultJson, SelectFolderResult.class);
            selectedFolderTextView.setText(selectedFolderResult.folderPath + " " + "(" + selectedFolderResult.folderType.toString() + ")");
        }
        else {
            selectedFolderTextView.setText(getString(R.string.none));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(resultCode) {
            case FOLDER_SELECTED:
                String selectedFolderResultJson = data.getExtras().getString(getString(R.string.prefs_selected_folder_result));
                SelectFolderResult selectedFolderResult = new Gson().fromJson(selectedFolderResultJson, SelectFolderResult.class);

                selectedFolderTextView.setText(selectedFolderResult.folderPath + " " + "(" + selectedFolderResult.folderType.toString() + ")");

                SharedPreferences prefs = getSharedPreferences(getString(R.string.shared_prefs_key), MODE_PRIVATE);
                prefs.edit().putString(getString(R.string.prefs_selected_folder_result), selectedFolderResultJson).apply();

                break;
        }
    }

    public boolean setUpStoragePermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            boolean hasRead = checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
            boolean hasWrite = checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;

            if (hasRead && hasWrite) {
                return true;
            } else {
                String[] permissions = null;

                if(!hasRead && !hasWrite) {
                    permissions = new String[] { Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE };
                }
                else if(!hasRead) {
                    permissions = new String[] { Manifest.permission.READ_EXTERNAL_STORAGE };
                }
                else if(!hasWrite) {
                    permissions = new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE };
                }

                ActivityCompat.requestPermissions(this, permissions, 1);

                return false;
            }
        }
        else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean permissionDenied = false;
        for (int grantResult : grantResults) {
            permissionDenied = permissionDenied || grantResult == PackageManager.PERMISSION_DENIED;
        }

        if(permissionDenied) {
            new AlertDialog.Builder(MainActivity.this)
                .setMessage("This app won't work without read/write permissions!")
                .setCancelable(true)
                .setPositiveButton(getString(android.R.string.ok), (dialog, which) -> {
                    finish();

                    dialog.dismiss();
                })
                .create().show();
        }
    }
}
