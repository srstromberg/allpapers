package com.stromberg.scott.allpapers.dropbox;

import android.app.WallpaperManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;

import com.dropbox.core.v2.files.FileMetadata;
import com.dropbox.core.v2.files.ListFolderResult;
import com.dropbox.core.v2.files.Metadata;
import com.stromberg.scott.allpapers.R;
import com.stromberg.scott.allpapers.model.ScaleType;
import com.stromberg.scott.allpapers.model.SelectFolderResult;
import com.stromberg.scott.allpapers.util.BitmapUtil;

import java.io.File;
import java.io.IOException;
import java.util.Random;

public class DropboxWallpaperUpdater {
  private final Display display;
  private Context mContext;
  private float screenWidth;
  private float screenHeight;

  public DropboxWallpaperUpdater(Context context) {
    mContext = context;

    display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
    Point size = new Point();
    display.getSize(size);
    screenWidth = size.x;
    screenHeight = size.y;
  }

  public void update(SelectFolderResult selectedFolderResult) {
    if(selectedFolderResult != null) {
      DropboxClientFactory.init(DropboxAuth.getOAuth2Token(mContext));
      PicassoClient.init(mContext, DropboxClientFactory.getClient());

      ListFolderTask listFolderTask = new ListFolderTask(DropboxClientFactory.getClient(), new ListFolderTask.Callback() {
        @Override
        public void onDataLoaded(ListFolderResult result) {
          int entryCount = result.getEntries().size();
          if(entryCount > 0) {
            int selectedEntry = new Random().nextInt(entryCount);
            Metadata metadata = result.getEntries().get(selectedEntry);

            if(metadata instanceof FileMetadata) {
              MimeTypeMap mime = MimeTypeMap.getSingleton();
              String ext = metadata.getName().substring(metadata.getName().indexOf(".") + 1);
              String type = mime.getMimeTypeFromExtension(ext);
              if (type != null && type.startsWith("image/")) {
                DownloadFileTask downloadFileTask = new DownloadFileTask(mContext, DropboxClientFactory.getClient(), new DownloadFileTask.Callback() {
                  @Override
                  public void onDownloadComplete(File result) {
                    WallpaperManager myWallpaperManager = WallpaperManager.getInstance(mContext);
                    try {
                      Log.d("Allpapers", "Original wallpaper name: " + result.getPath());

                      File downloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                      File file = new File(downloads, "wallpaper-temp");

                      if(file.exists()) {
                        file.delete();
                      }

                      result.renameTo(file);

                      SharedPreferences prefs = mContext.getSharedPreferences(mContext.getString(R.string.shared_prefs_key), mContext.MODE_PRIVATE);

                      BitmapFactory.Options options = new BitmapFactory.Options();
                      options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                      Bitmap bitmap = BitmapFactory.decodeFile(file.getPath(), options);

                      int scaleType = prefs.getInt(mContext.getString(R.string.prefs_scale_type), ScaleType.CENTER.getAsInt());

                      if(scaleType == ScaleType.FIT.getAsInt()) {
                        bitmap = BitmapUtil.createFitBitmap(bitmap, screenWidth, screenHeight, true);
                      }
                      else if(scaleType == ScaleType.CENTER.getAsInt()) {
                        bitmap = BitmapUtil.createCenterBitmap(bitmap, (int) screenWidth, (int) screenHeight);
                      }

                      myWallpaperManager.setBitmap(bitmap);

                      Log.d("Allpapers", "Wallpaper set!");
                    } catch (IOException e) {
                      Log.d("Allpapers", "Failed to set wallpaper.");
                    }
                  }

                  @Override
                  public void onError(Exception e) {
                    Log.d("Allpapers", "Download error: failed to download file.");
                  }
                });

                downloadFileTask.execute((FileMetadata) metadata);
              } else {
                Log.d("Allpapers", "Download error: attempted to download a file that was not an image.");
              }
            }
            else {
              Log.d("Allpapers", "Download error: attempted to download a folder.");
            }
          }
          else {
            Log.d("Allpapers", "Download error: no files found.");
          }
        }

        @Override
        public void onError(Exception e) {
          Log.d("Allpapers", "Download error: failed to fetch folder.");
        }
      });

      listFolderTask.execute(selectedFolderResult.folderPath);
    }
    else {
      Log.d("Allpapers", "Download error: no folder selected.");
    }
  }
}
