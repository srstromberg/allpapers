package com.stromberg.scott.allpapers.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.stromberg.scott.allpapers.R;

import java.util.Calendar;
import java.util.Timer;

public class TimerService extends Service {
  public static final int CHANGE_WALLPAPER = 3000;
  public static final int RESTART_TIMER = 3001;

  public static boolean isRunning = false;

  private Timer timer = new Timer();
  private BroadcastReceiver mReceiver;

  @Override
  public void onCreate() {
    super.onCreate();
    startService();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    shutdownService();
  }

  private void startService() {
    isRunning = true;
    Log.d("Allpapers", "Starting service");

    int updateInterval = getSharedPreferences(getString(R.string.shared_prefs_key), MODE_PRIVATE).getInt(getString(R.string.prefs_wallpaper_change_interval), Integer.parseInt(getString(R.string.default_wallpaper_change_interval)));

    AlarmManager mainUpdateManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
    mainUpdateManager.setRepeating(AlarmManager.RTC, Calendar.getInstance().getTimeInMillis(), updateInterval, createOnReceiveBroadcastIntent(this, getString(R.string.filter_action_change_wallpaper)));
  }

  private void shutdownService() {
    isRunning = false;
    Log.d("Allpapers", "Stopping service");

    if (timer != null) {
      timer.cancel();
    }
  }

  @Nullable
  @Override
  public IBinder onBind(Intent intent) {
    return null;
  }

  private PendingIntent createOnReceiveBroadcastIntent(Context context, String intentPackage) {
    Intent intent = new Intent(context, TimerReceiver.class);
    intent.setAction(intentPackage);
    intent.putExtra("RequestCode", CHANGE_WALLPAPER);
    PendingIntent pendingIntent = PendingIntent.getBroadcast(context, CHANGE_WALLPAPER, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    return pendingIntent;
  }
}