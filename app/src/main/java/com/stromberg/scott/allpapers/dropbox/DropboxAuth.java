package com.stromberg.scott.allpapers.dropbox;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.dropbox.core.android.AuthActivity;
import com.stromberg.scott.allpapers.R;

public class DropboxAuth {
    public static void startOAuth2Authentication(Context context) {

        if (!AuthActivity.checkAppBeforeAuth(context, context.getString(R.string.app_key), true)) {
            return;
        }

        Intent intent =  AuthActivity.makeIntent(context, context.getString(R.string.app_key), "www.dropbox.com", "1");
        if (!(context instanceof Activity)) {
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        context.startActivity(intent);
    }

    public static String getOAuth2Token(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(context.getString(R.string.shared_prefs_key), context.MODE_PRIVATE);
        String accessToken = prefs.getString(context.getString(R.string.prefs_access_token), null);
        if (accessToken != null) {
            return accessToken;
        }

        Intent data = AuthActivity.result;

        if (data == null) {
            return null;
        }

        String token = data.getStringExtra(AuthActivity.EXTRA_ACCESS_TOKEN);
        String secret = data.getStringExtra(AuthActivity.EXTRA_ACCESS_SECRET);
        String uid = data.getStringExtra(AuthActivity.EXTRA_UID);

        if (token != null && !token.equals("") &&
                secret != null && !secret.equals("") &&
                uid != null && !uid.equals("")) {
            prefs.edit().putString(context.getString(R.string.prefs_access_token), secret).apply();

            return secret;
        }

        return null;
    }

}